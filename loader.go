package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

func loadWebsites() []*Website {
	file, err := os.Open("websites.txt")
	if os.IsNotExist(err) {
		file, err := os.Create("websites.txt")
		if err != nil {
			fmt.Println(err)
		}
		n, err := file.WriteString(defaultWebsites)
		if err != nil {
			fmt.Print(err)
		}
		fmt.Println("new file ", n)
		return parseWebsite(defaultWebsites)
	} else if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	return parseWebsite(string(content))

}

func parseWebsite(str string) []*Website {
	var result []*Website
	for _, line := range strings.Split(str, "\n") {
		if !strings.HasPrefix(line, "//") && len(line) > 0 {
			infos := strings.Split(line, " ")
			if len(infos) < 2 {
				fmt.Println("websites.txt is incorrect (make sure check intervals are good)")
			} else {
				interval, err := time.ParseDuration(infos[1])
				if err != nil {
					fmt.Println("websites.txt is incorrect (make sure check intervals are good)")
				}
				result = append(result, &Website{url: infos[0], checkInterval: interval})
			}
		}
	}

	return result
}
