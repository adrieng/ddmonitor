![Capture d'écran](https://preview.ibb.co/iXQJs0/capture.png")

# Install

## Windows

Either open directly `ddmonitor.exe` or run it form command line:
```
C:/.../ddmonitor/windows> ddmonitor
```

## OSX

To be able to use a custom list of websites, run the program from a terminal:
```
____:osx ___$ ./ddmonitor
```


## Linux

Run the program from a terminal:
```
______:linux$ ./ddmonitor
```

## Edit websites list

The websites list is stored in websites.txt.
websites.txt is created at first launch of the program.

Edit the file with any text editor, just respect the following syntax.
```
websiteUrl checkInterval
```
with one website per line and  `checkInterval` as time string: `500ms`, `1s`, `2s`, `1m` ...

For instance:
```
https://github.com 1s
https://datadog.com 2s
```

# Description

For each Website:
* Each check goes in the websites `checksBuffer`
* One goroutine is responsible for agregating the data from the `checksBuffer` and computing stats, **every 10s**
* The computed stats are put in a `statBuffer` and in two `statsPile` that represent the last 2 and 10 minutes of data
* Every time a new `Stats` goes in a `statsPile`, the pile compute the new metrics
* One goroutine is responsible for computing metrics out of the `statsBuffer` and putting it in a `statsPile` that represent one hour of data, **every minute**
* An alert is sent if the availibility state changed for the past 2 minutes

In short we compute the last 10s of checks every 10s, and the last minute of stats every minute.
Memory usage will be constant : 12 (two minutes) + 60 (10 minutes) + 60 (1 hour) `Stats` objects (computed stats) represents one hour of data.

## Metrics

Stats are computed for the next metrics over 2min, 10min and 1h timeframes:
* Website availability `availability`
* Average response time `avgRespTime`
* Maximum response time `maxRespTime`
* Reponse codes counts `codesCount` : `2xx`, `4xx`, `5xx` and request error `err`
* Availability state : `Up` if availability > 80% else `Down`
	
	
# Next steps and improvements :

To improve the program:
* Write tests
* Use only one pile to store 2 minutes and 10 minutes data
* Possibility to edit websites list directly into the program
* Compute new metrics such download and upload times, arrival times of first and last paquets
* Let the user choose timeframes and metrics
* Analyse metrics to identify problems with more precision and launch targeted alerts
* Propose a mode that display only suspicious metrics (to monitor lots of websites)