package main

import (
	"sync"
	"time"
)

var codesRef = map[string]int{"2xx": 0, "4xx": 1, "5xx": 2, "err": 3}

//Stats type represents the computed metrics
type Stats struct {
	sampleSize   int
	avgRespTime  time.Duration
	maxRespTime  time.Duration
	codesCount   [4]int
	availability float32
	isAvailable  bool
}

//totalCodesCount return the total amount of non error request
func (stats *Stats) totalCodesCount() int {
	result := stats.codesCount[codesRef["2xx"]] + stats.codesCount[codesRef["4xx"]] + stats.codesCount[codesRef["5xx"]]
	return result
}

//StatsBuffer type is filled every 10s by computeChecks and emptied every minute by computeStats
type StatsBuffer struct {
	stats []Stats
	mux   sync.Mutex
}

//clear the buffer
func (buf *StatsBuffer) clear() {
	buf.stats = buf.stats[:0]
}

//add a Stats to the buffer
func (buf *StatsBuffer) add(stats Stats) {
	buf.mux.Lock()
	buf.stats = append(buf.stats, stats)
	buf.mux.Unlock()
}

//return all Stats from the buffer and clear it
func (buf *StatsBuffer) popAll() []Stats {
	buf.mux.Lock()
	defer buf.mux.Unlock()
	defer buf.clear()
	return buf.stats
}

//StatsPile type stores the stats history for a given timeframe
//current position is stored in cursor
type StatsPile struct {
	size          int
	stats         []Stats
	cursor        int
	computedStats Stats
	alertsOn      bool
	alertsBuffer  AlertsBuffer
	firstAlert    bool
}

//init with the size which correspond to timeframe (eg 12 * 10s = 2m)
func (pile *StatsPile) init(size int) {
	pile.size = size
	pile.stats = make([]Stats, size)
	pile.firstAlert = true
}

//popAndPush replace the oldest stats from the pile with the new one
func (pile *StatsPile) popAndPush(newStats Stats) {
	oldStats := pile.stats[pile.cursor]
	pile.stats[pile.cursor] = newStats

	pile.cursor++
	if pile.cursor >= len(pile.stats) {
		pile.cursor = 0
	}

	//compute the stats for the updated pile
	pile.compute(newStats, oldStats)
}

//compute the metrics for a collection of stats given the added one and the removed one
func (pile *StatsPile) compute(newStats, oldStats Stats) {
	result := &pile.computedStats
	prevCodesCount := result.totalCodesCount()
	prevIsAvailable := result.isAvailable

	//compute sampleSize
	result.sampleSize += newStats.sampleSize - oldStats.sampleSize
	//compute codesCount
	for i := range result.codesCount {
		result.codesCount[i] += newStats.codesCount[i] - oldStats.codesCount[i]
	}

	//compute average response time
	if result.totalCodesCount() > 0 {
		result.avgRespTime = (result.avgRespTime*time.Duration(prevCodesCount) - oldStats.avgRespTime*time.Duration(oldStats.totalCodesCount()) + newStats.avgRespTime*time.Duration(newStats.totalCodesCount())) / time.Duration(result.totalCodesCount())

	} else {
		result.avgRespTime = 0
	}

	//compute max response time
	if result.maxRespTime == oldStats.maxRespTime {
		m := 0 * time.Millisecond
		for _, s := range pile.stats {
			if s.maxRespTime > m {
				m = s.maxRespTime
			}
		}
		result.maxRespTime = m

	} else if newStats.maxRespTime > result.maxRespTime {
		result.maxRespTime = newStats.maxRespTime
	}

	//compute availability
	if result.sampleSize > 0 {
		result.availability = float32(result.codesCount[codesRef["2xx"]]) / float32(result.sampleSize)
	} else {
		result.availability = 0
	}

	//Alerting logic
	//Alert is authorized only if 2 minutes are elapsed
	canAlert := pile.alertsOn && time.Since(startTime) > alertDelay
	//if availabilty is above 80% for the past 2min, alert for recovery
	if result.availability > 0.8 {
		if canAlert && (!prevIsAvailable || pile.firstAlert) {
			pile.alertsBuffer.add(Alert{category: "up", availability: result.availability, timestamp: time.Now()})
			pile.firstAlert = false
		}
		//update availability state (isAvailable)
		result.isAvailable = true
	} else {
		//if availabilty is less than 80% for the past 2min, alert for website down
		if canAlert && (prevIsAvailable || pile.firstAlert) {
			pile.alertsBuffer.add(Alert{category: "down", availability: result.availability, timestamp: time.Now()})
			pile.firstAlert = false
		}
		//update availability state (isAvailable)
		result.isAvailable = false
	}

}
