package main

import (
	"net/http"
	"sync"
	"time"
)

var client = &http.Client{
	Timeout: 5 * time.Second,
}

//Check type is the object corresponding to one check response
type Check struct {
	ok           bool
	responseTime time.Duration
	statusCode   int
}

//ChecksBuffer type stores the new checks for later use by computing logic
type ChecksBuffer struct {
	checks []Check
	mux    sync.Mutex
}

//clear the buffer
func (buf *ChecksBuffer) clear() {
	buf.checks = buf.checks[:0]
}

//add a Check to the buffer
func (buf *ChecksBuffer) add(check Check) {
	buf.mux.Lock()
	buf.checks = append(buf.checks, check)
	buf.mux.Unlock()
}

//return all Checks from the buffer and clear it
func (buf *ChecksBuffer) popAll() []Check {
	buf.mux.Lock()
	defer buf.mux.Unlock()
	defer buf.clear()
	return buf.checks
}

//launch a check on the given website every time the check interval is elapsed
func loopCheck(website *Website) {
	for {
		go checkOnce(website)
		time.Sleep(website.checkInterval)
	}
}

//do a check for the given website
func checkOnce(website *Website) {
	start := time.Now()
	//launch a request to the website
	resp, err := client.Get(website.url)
	//compute the time spent by the request
	elapsedTime := time.Since(start)
	//if an error occured code error
	if err != nil {
		//return a failed check
		website.checksBuffer.add(Check{false, 0, 0})
	} else {
		//http code 2xx, 4xx and 5xx, return the corresponding check object
		website.checksBuffer.add(Check{true, elapsedTime, resp.StatusCode})
	}
}
