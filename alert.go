package main

import (
	"fmt"
	"time"
)

var alertDelay = 2 * time.Minute

//Alert type
type Alert struct {
	category     string
	availability float32
	timestamp    time.Time
}

//AlertsBuffer type stores the alerts launch by stats computing for future display
type AlertsBuffer struct {
	alerts []Alert
}

//clear the buffer
func (buf *AlertsBuffer) clear() {
	buf.alerts = buf.alerts[:0]
}

//add an Alert to the buffer
func (buf *AlertsBuffer) add(alert Alert) {
	buf.alerts = append(buf.alerts, alert)
}

//return all Alerts from the buffer and clear it
func (buf *AlertsBuffer) popAll() []Alert {
	defer buf.clear()
	return buf.alerts
}

//convert an alert object to a displayable string
func alertToStr(website *Website, alert *Alert) string {
	switch alert.category {
	case "up":
		return fmt.Sprintf(setColor("Website %v is up. availability=%v%v, time=%v\n", "greentext"), website.url, int(100*alert.availability), "%%", alert.timestamp.Truncate(time.Second))
	case "down":
		return fmt.Sprintf(setColor("Website %v is down. availability=%v%v, time=%v\n", "redtext"), website.url, int(100*alert.availability), "%%", alert.timestamp.Truncate(time.Second))
	default:
		return "Received alerts of no type"
	}
}
