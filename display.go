package main

import (
	"fmt"
	"sync"
	"text/tabwriter"
	"time"

	colorable "github.com/mattn/go-colorable"
)

//To enable ANSI colors on windows cmd
var stdout = colorable.NewColorableStdout()

//create the output writer to display a table
var output = tabwriter.NewWriter(stdout, 0, 10, 3, ' ', 0)
var writerMutex sync.Mutex

//clearTable place the cursor at the beigining of table and clear the page for rewriting
func clearTable(nbOfWebsites int) {
	tableHeight := 4 + 6*nbOfWebsites
	command := fmt.Sprintf("\x1b[%vA\x1b[J", tableHeight)
	fmt.Fprint(stdout, command)
}

//newScreen move the cursor to a new blank screen
func newScreen() {
	fmt.Fprint(stdout, "\x1b[2J")
}

//setColor apply color codes for background and text and return the formatted string
func setColor(str string, color string) string {
	switch color {
	case "black": //black background & white text
		return "\x1b[40m" + "\x1b[37m" + str
	case "white": //white background & black text
		return "\x1b[47m" + "\x1b[30m" + str
	case "cyan":
		return "\x1b[46m" + "\x1b[30m" + str
	case "green":
		return "\x1b[42m" + "\x1b[30m" + str
	case "red":
		return "\x1b[41m" + "\x1b[30m" + str
	case "greentext":
		return "\x1b[40m" + "\x1b[32m" + str
	case "redtext":
		return "\x1b[40m" + "\x1b[31m" + str
	default:
		return "\x1b[40m" + "\x1b[37m" + str
	}
}

func updateTable(websites []*Website) {

	writerMutex.Lock()
	clearTable(len(websites))
	showTable(websites)
	writerMutex.Unlock()
}

//showTable print each line of the table to output using tabwriter
func showTable(websites []*Website) {

	elapsedTime := time.Since(startTime)

	//display new alerts
	for _, website := range websites {
		for _, alert := range website.stats2m.alertsBuffer.popAll() {
			fmt.Fprintf(output, alertToStr(website, &alert))
		}
	}

	//program info
	fmt.Fprintf(output, setColor("\nYou can edit the list of monitored websites in websites.txt\n", "black"))
	fmt.Fprintf(output, setColor("Elapsed Time : %v\n\n", "black"), elapsedTime.Truncate(time.Second))

	//websites metrics
	for _, website := range websites {

		//headers
		fmt.Fprintf(output, setColor("%s  -   check interval : %v\n", "white"), website.url, website.checkInterval)

		fmt.Fprintf(output, setColor("Timeframe\tAvailability\tAvgRespTime\tMaxRespTime\t2xx\t4xx\t5xx\terr\ttotal \tState  \n", "cyan"))

		//metrics
		timeframes := [3]string{"2m", "10m", "1h"}
		//for each timeframe print a line
		for i, stats := range [3]*Stats{&website.stats2m.computedStats, &website.stats10m.computedStats, &website.stats1h.computedStats} {
			if stats.sampleSize > 1 {
				//format and print metrics
				fmt.Fprintf(output, setColor("%v\t%v%%\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t", "black"), timeframes[i], int(100*stats.availability), stats.avgRespTime.Truncate(100*time.Microsecond), stats.maxRespTime.Truncate(100*time.Microsecond), stats.codesCount[codesRef["2xx"]], stats.codesCount[codesRef["4xx"]], stats.codesCount[codesRef["5xx"]], stats.codesCount[codesRef["err"]], stats.sampleSize)
				//availability markers
				if stats.isAvailable {
					fmt.Fprintf(output, setColor("  Up  ", "green"))
				} else {
					fmt.Fprintf(output, setColor(" Down ", "red"))
				}
				fmt.Fprintf(output, setColor("\n", "black"))

			} else {
				//no data
				fmt.Fprintf(output, setColor("%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t\n", "black"), timeframes[i], "Nan", "Nan", "Nan", "Nan", "Nan", "Nan", "Nan", "Nan")
			}

		}

		fmt.Fprintf(output, setColor("\n", "black"))

	}

	output.Flush()

}
