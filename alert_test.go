package main

import (
	"testing"
	"time"
)

//Test that an alert is sent when new data make the availibility change
func TestAlertLogic(t *testing.T) {
	//authorized alerts to be launched immmediatly
	alertDelay = 0 * time.Minute

	//create mock data
	var stats = []Stats{Stats{sampleSize: 10, codesCount: [4]int{9, 1, 0, 0}}, Stats{sampleSize: 10, codesCount: [4]int{9, 0, 1, 0}}}
	var computedStats = Stats{20, time.Second, time.Second, [4]int{18, 1, 1, 0}, 0.9, true}
	var alertsBuffer AlertsBuffer
	var statsPile = StatsPile{2, stats, 0, computedStats, true, alertsBuffer, false}

	//send new data which should change the stats trigger an alert
	//alert website down should be sent
	statsPile.popAndPush(Stats{sampleSize: 100, codesCount: [4]int{9, 1, 0, 0}})
	//alert website up should be up
	statsPile.popAndPush(Stats{sampleSize: 1000, codesCount: [4]int{1000, 0, 0, 0}})

	//restore params
	alertDelay = 2 * time.Minute

	//return error if anything goes wrong
	if alerts := statsPile.alertsBuffer.popAll(); len(alerts) < 2 {
		t.Error("Should have sent 2 alerts")
	} else {
		if alerts[0].category != "down" {
			t.Error("Alert is of wrong type (should be down)")

		}
		if alerts[1].category != "up" {
			t.Error("Alert is of wrong type (should be up)")

		}
	}

}
