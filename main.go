package main

import (
	"time"
)

const defaultWebsites string = "//You can add your websites with syntax :\n//url check_interval\n//(separated by a space, each line is a website, checksinterval is 500ms, 1s, 5s, 20s ...)\nhttps://www.wikipedia.org 1s\nhttps://www.datadog.com 2s"

var startTime time.Time

//Website type gives the target and the store all the linked stats
type Website struct {
	url           string
	checkInterval time.Duration
	checksBuffer  ChecksBuffer
	statsBuffer   StatsBuffer
	stats2m       StatsPile
	stats10m      StatsPile
	stats1h       StatsPile
}

//stats2m is the stats pile for 2 minutes of checks (computed every 10s, hence the size of 12)
//stats10m is the stats pile for 10 minutes of checks (computed every 10s)
//stats1h is the stats pile for 1 hour of checks (computed every minute)
func (w *Website) init() {
	w.stats2m.init(12)
	w.stats2m.alertsOn = true
	w.stats10m.init(60)
	w.stats1h.init(60)
}

func main() {

	//new terminal screen
	newScreen()

	//load the websites and check intervals form websites.txt
	websites := loadWebsites()

	//save start time for later
	startTime = time.Now()

	//launch each website check routine (check the website with given check interval)
	for _, website := range websites {
		website.init()
		go loopCheck(website)
	}
	//launch routines for compute and display updates
	go loopUpdate10s(websites)
	go loopUpdate1m(websites)

	//prevent exit
	select {}
}

//compute the checks received in last 10s and update table display
func loopUpdate10s(websites []*Website) {
	for {
		start := time.Now()
		for _, website := range websites {
			computeChecks(website)
		}
		updateTable(websites)
		time.Sleep(10*time.Second - time.Since(start))
	}
}

//compute the stats from the past minute and update table display
func loopUpdate1m(websites []*Website) {
	for {
		start := time.Now()
		for _, website := range websites {
			computeStats(website)
		}
		updateTable(websites)
		time.Sleep(time.Minute - time.Since(start))
	}
}

//get the checks from current website buffer and agregate them into one stats object
func computeChecks(website *Website) {

	checks := website.checksBuffer.popAll()
	var stats Stats
	var respTimeSum time.Duration

	for _, check := range checks {

		//update stats data with current check
		//sample size
		stats.sampleSize++
		if check.ok {
			//codes count
			switch c := check.statusCode / 100; c {
			case 2:
				stats.codesCount[codesRef["2xx"]]++
			case 4:
				stats.codesCount[codesRef["4xx"]]++
			case 5:
				stats.codesCount[codesRef["5xx"]]++
			}

			respTimeSum += check.responseTime
			//max response time
			if check.responseTime > stats.maxRespTime {
				stats.maxRespTime = check.responseTime
			}
		} else {
			stats.codesCount[codesRef["err"]]++

		}
	}
	//compute average response time
	if stats.totalCodesCount() > 0 {
		stats.avgRespTime = respTimeSum / time.Duration(stats.totalCodesCount())
	}

	//save computed stats in buffer for later use by computeStats
	website.statsBuffer.add(stats)
	//save computed stats in 2 minutes and 10 minutes stats piles for later display
	website.stats2m.popAndPush(stats)
	website.stats10m.popAndPush(stats)
}

//get the stats from current website buffer and agregate them into one new stats object
func computeStats(website *Website) {

	//get the stats from buffer
	stats := website.statsBuffer.popAll()
	var result Stats

	//iterate over available stats
	for _, data := range stats {

		//update result with current stats
		//sample size
		result.sampleSize += data.sampleSize
		//code counts
		for i := range result.codesCount {
			result.codesCount[i] += data.codesCount[i]
		}
		//max response time
		if data.maxRespTime > result.maxRespTime {
			result.maxRespTime = data.maxRespTime
		}
	}
	//compute average response time
	if result.totalCodesCount() > 0 {
		for _, data := range stats {
			result.avgRespTime += data.avgRespTime * time.Duration(data.totalCodesCount()) / time.Duration(result.totalCodesCount())
		}
	}

	//save computed stats in 1 hour stats pile for later display
	website.stats1h.popAndPush(result)
}
